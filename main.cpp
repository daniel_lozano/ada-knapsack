#include <iostream>
#include <fstream>
#include <vector>
#include <string.h>
#include <ctime>

using namespace std;

typedef pair<int,int> _pair;
typedef vector<_pair> _vector;

int varKnapsackSize;
int varElementsSize;

void funReadFile(const char * parFilename, _vector & parVector)
{
	ifstream varFileStream;
	varFileStream.open(parFilename);
	int val, wei;

	varFileStream >> varKnapsackSize >> varElementsSize;
	while(true)
	{
		varFileStream >> val >> wei;
		if(varFileStream.eof() ) break;
		parVector.push_back(make_pair(val,wei));
	}		
}

int funSolveKnapsack(_vector & parVector)
{
	int int_s = sizeof(int);
	int * Arr[2], j;
	bool index = true;

	Arr[0] = new int[varKnapsackSize+1];
	Arr[1] = new int[varKnapsackSize+1];

	memset(Arr[0] , 0, int_s*varKnapsackSize);
	memset(Arr[1] , 0, int_s*varKnapsackSize);

	for(auto ite=parVector.begin() ; ite!=parVector.end() ; ++ite, index^=1)
	{
		for(j=(*ite).second ; j<=varKnapsackSize ; ++j)
		{				
			if(not(Arr[index][j] < ( Arr[index^1][j]=Arr[index][j-(*ite).second] + (*ite).first) ))
				Arr[index^1][j] = Arr[index][j];
		}
		memcpy(Arr[index^1], Arr[index], int_s*((*ite).second-1));
	}

	return Arr[1][varKnapsackSize];
}

int main()
{
	_vector varVector;
	funReadFile("knapsack1.txt", varVector);
	
	const clock_t varBeginTime = clock();	
	cout << "knapsack1 : " << funSolveKnapsack(varVector) << endl;
	cout << float(clock() - varBeginTime)/CLOCKS_PER_SEC << " segundos"<< endl;
	
	varVector.clear();

	funReadFile("knapsack_big.txt", varVector);
	const clock_t varBeginTime2 = clock();
	
	cout << "knapsack_big : " << funSolveKnapsack(varVector) << endl;
	cout << float(clock() - varBeginTime2)/CLOCKS_PER_SEC << " segundos"<< endl;

	return 0;
}

